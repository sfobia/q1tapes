<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// Try server hostname
if (!defined('WP_ENV')) {
    // Set environment based on hostname
    include 'wp-config.env.php';
}

// Load config file for current environment
include 'wp-config.' . WP_ENV . '.php';

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'FYn^>}4BJQkrz@,8FNRYrz@|8FNRYu+*<AILfmqy{26EXfiqy*3AEMfmuy^3AEMUn');
define('SECURE_AUTH_KEY',  'q6;PHeXi*xHAXPme+u*6LEfqm.<ITPbyu^AMbTqjdZwo|~5:KCZSp-s[_91OGdW');
define('LOGGED_IN_KEY',    'S~;#H9WOaxp#_92PHv}>FRJkcz|@4}JCZRok!z0[4RNkdCZRsk!-1[C4RKOkdwo|~');
define('NONCE_KEY',        ',Nz>}BRr*;]Ti+];EXm;ETiy{AETm${Aj$IXCShw#9Odtx#5Ket_1HWl+]SPa+#_2');
define('AUTH_SALT',        'Kpt_1v,}BMc$}73JUj,4FURr>R[CREHXm+{ETfu.6Mbq3IXn$EUXq^BUjydt_5');
define('SECURE_AUTH_SALT', ';atl_+;#D5SLiaxp_5;Hatm*2]DWLi+q#AMr^3QJgYvn,z0>F7UNkczr>B4RJgVr|');
define('LOGGED_IN_SALT',   '-5hs~Odp~[5K-]5Kar^}BQcr!Rgr!}CNc}CNZoz[Zo@[8N*{AQfq^Qbq${BMbr^UF');
define('NONCE_SALT',       '9FUngz}^7QFYr,z0JBUNg@r,B0JgVo!4>CZNgzJds|zLfy{*My<EQnf$uUj$<B');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD', 'direct');
define('WP_MEMORY_LIMIT', '128M');

define( 'PCLZIP_TEMPORARY_DIR', dirname(__FILE__) . '/wp-content/temp/' );


if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
    $_SERVER['HTTPS'] = 'on';


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
