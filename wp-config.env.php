<?php
/**
 * Setup environments
 *
 * Set environment based on the current server hostname, this is stored
 * in the $hostname variable
 *
 * You can define the current environment via:
 *     define('WP_ENV', 'production');
 *
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */

// Define site host
if (isset($_SERVER['X_FORWARDED_HOST']) && !empty($_SERVER['X_FORWARDED_HOST'])) {
    $hostname = $_SERVER['X_FORWARDED_HOST'];
} else {
    $hostname = $_SERVER['HTTP_HOST'];
}

// Set environment based on hostname
switch ($hostname) {

    case 'dev.q1tapes.com':
        define('WP_ENV', 'development');
        define('TRACKING', false );
        break;

    case 'staging.q1tapes.com':
        define('WP_ENV', 'staging');
        define('TRACKING', true );
        break;

    case 'www.q1tapes.com':
    default:
        define('WP_ENV', 'production');
        define('TRACKING', true );
        break;
}

define('WP_ALLOW_MULTISITE', true);

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', $hostname );
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
