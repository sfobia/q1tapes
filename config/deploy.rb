# config valid only for Capistrano 3.4
lock '3.4.1'

# require Slack config
# require './config/slack'


############################################
# Setup project
############################################

set :application, "Q1tapes"
set :repo_url, "git@bitbucket.org:sfobia/q1tapes.git"

set :scm, :git

# set :git_strategy, SubmoduleStrategy

############################################
# Setup Capistrano
############################################

set :log_level, :info
set :use_sudo, false

set :ssh_options, {
  forward_agent: true
}

set :keep_releases, 5

############################################
# Linked files and directories (symlinks)
############################################

# set :linked_files, %w{wp-config.php .htaccess}
# set :linked_files, %w{sitemap-news.xml robots.txt}
set :linked_dirs, %w{wp-content/uploads wp-content/cache wp-content/updraft wp-content/w3tc-config}

namespace :deploy do

#  desc "create WordPress files for symlinking"
#  task :create_wp_files do
#    on roles(:app) do
#      execute :touch, "#{shared_path}/wp-config.php"
#      execute :touch, "#{shared_path}/.htaccess"
#    end
#  end

#  after 'check:make_linked_dirs', :create_wp_files


  task :permission_sitemap_news do
#  	on roles(:app) do
#  	  execute :chmod, "775 #{shared_path}/sitemap-news.xml"
#  	end
  end

  # after :finished, :create_robots, :permission_sitemap_news
  after :finished, :permission_sitemap_news
  # after :create_robots, :create_w3tc_file
  after :finishing, "deploy:cleanup"

end
