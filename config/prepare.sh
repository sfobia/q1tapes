#!/bin/bash
#####################################
site_name=`printf '%s\n' "${PWD##*/}"`
#if [ -z ${1} ]; then 
#       echo "Specificare un nome di progetto" 
#       exit 1 
#    else 
#       site_name=${1}
#fi

if [ -z ${1} ]; then 
       echo "Specificare un url per raggiungere il sito in locale" 
       exit 1 
    else 
       site_url=${1}
fi

####################################
# add a submodule to git project   #
# $1 repo                          #
# $2 module                        #
# $3 version : optional            #
# $3 folder  : optional            #
#              default like module #
####################################
addSubModule(){
    if [ -z ${1} ]; then 
       echo "Impossibile aggiungere il modulo se non viene specificato il repository" 
       exit 1 
    else 
      repo=${1}
    fi
    
    if [ -z ${2} ]; then 
       echo "Impossibile aggiungere il modulo se non viene specificato il nome" 
       exit 1 
    else 
      module=${2}
     
    fi
    
    if [ -z ${4} ]; then folder=$module ; else folder=${4} ; fi
   
  
    git submodule add $repo/$module.git content/plugins/$folder
    cd content/plugins/$folder
    if [ -z ${3} ]; then  git checkout ; else  git checkout ${3} ; fi
    cd ../../..
    git add content/plugins/$folder
    git commit -m "aggiunto plugin $module"
    git push origin master
}

######################################################
# remove a submodule from git project                #
# $1 module_name, es: content/advanced-custom-fields #
######################################################
removeSubModule(){  
  if [ -z ${1} ]; then 
     echo "Impossibile rimuovere il modulo se non viene specificato" 
     exit 1 
  else 
    module_name=${1}
  fi
  
  git submodule deinit $module_name    
  git rm $module_name
  git rm --cached $module_name  
  rm -rf .git/modules/$module_name
}

createVirtualHostConfFile(){
   path=`pwd`
   clean_path=`echo "${path}" | sed 's?/?\\\\/?g'`
   clean_site_url=`echo $site_url | sed 's/\./\\\./g'`
   clean_site_name=`echo $site_name | sed 's/\./\\\./g'`
   find "config/virtual-host.conf" -type f -exec sed -i "s?@@document_root@@?$clean_path?g;s?@@site_name@@?$clean_site_name?g;s?@@site_url@@?$clean_site_url?g;" "{}" ';'
   sudo ln -s $path/config/virtual-host.conf /etc/apache2/sites-enabled/$site_name.conf
}

createEclipseProject(){
   path=`pwd`
   clean_path=`echo "${path}" | sed 's?/?\\\\/?g'`
   clean_site_url=`echo $site_url | sed 's/\./\\\./g'`
   clean_site_name=`echo $site_name | sed 's/\./\\\./g'`
   find config/templates/eclipse -type f -exec sed -i "s?@@document_root@@?$clean_path?g;s?@@site_name@@?$clean_site_name?g;s?@@site_url@@?$clean_site_url?g;" "{}" ';'
   mv config/templates/eclipse/.* .
}

createSecretKeys(){
    curl -s -k -O "https://api.wordpress.org/secret-key/1.1/salt"
}

####################################
# Prepara il progetto per eclipse  #
####################################
createEclipseProject

####################################
# Crea il virtual-host             #
####################################
createVirtualHostConfFile

createSecretKeys

####################################
# Aggiunge Wordpress               #
####################################
rm -rf .git
git init
git remote rm origin
rm -rf wordpress
git submodule add git://github.com/WordPress/WordPress.git wordpress
cd wordpress 
git checkout 4.4.2
cd ..
git add -A
git commit -m "Inital commit"

####################################
# Moduli Wordpress                 #
####################################
#addSubModule https://github.com/elliotcondon acf 4.4.3 advanced-custom-fields
#addSubModule https://github.com/soderlind acf-field-date-time-picker 2.0.18.1 acf-field-date-time-picker
#addSubModule https://github.com/kenmoini akismet 3.1.1
#addSubModule https://github.com/parisholley wordpress-fantastic-elasticsearch 3.1.1 fantastic-elasticsearch
addSubModule https://github.com/siteorigin siteorigin-panels 2.2.1
#addSubModule https://github.com/qTranslate-Team qtranslate-x 3.2.9
addSubModule https://github.com/crowdfavorite-mirrors wp-w3-total-cache 0.9.4.1 w3-total-cache
addSubModule https://github.com/Yoast wordpress-seo 2.3.5 wordpress-seo
# MANCA QUESTO PLUGIN wp-dropdown-hierarchial-category-ui

####################################
# Moduli Arnoldo Mondadori Editore #
####################################
#manca akismet
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-acf-repeater 1.0.1 #ok
addSubModule git@gitlab.mondadori.it:digital-innovation ame-cdn-versioning 1.0.2 #svn trunk
addSubModule git@gitlab.mondadori.it:digital-innovation ame-connect 1.0.11 #svn branch panorama-form-1.0.4
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-gallery 2.0.1 #svn branch salepepe_2015
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-manual-image-crop 1.1.0 # svn 1.1.0
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-piattaformavideo 1.0.0 #svn trunk 
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-schedule-update 1.0.0 #svn trunk
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-sirtrevor 1.0.4 #git_1.0.4
addSubModule git@gitlab.mondadori.it:digital-innovation ame-site 1.5.2 #tags git
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-siteorigin-panels 1.0.0 #svn trunk

#addSubModule git@gitlab.mondadori.it:digital-innovation fnc-api 1.0.0
#addSubModule git@gitlab.mondadori.it:digital-innovation basic_auth 1.0.0

#rimappatura per porting sito da vecchi wordpress
#ln -s content wp-content
