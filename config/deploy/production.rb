############################################
# Setup Server
############################################

set :stage, :production
set :stage_url, "www.q1tapes.com"
server "ne146.aws.neen.it", user: "q1tapes", roles: %w{web app db}

set :deploy_to, "/var/www/q1tapes.com/deploy"

############################################
# Setup Git
############################################

set :branch, "master"

############################################
# Extra Settings
############################################

#specify extra ssh options:

set :ssh_options, {
    auth_methods: %w(password),
    password: 'aezoh8raiD',
    user: 'q1tapes',
}