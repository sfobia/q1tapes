############################################
# Setup Server
############################################

set :stage, :staging
set :stage_url, "staging.q1tapes.com"
server "ne145.aws.neen.it", user: "st_q1tapes", roles: %w{web app db}
set :deploy_to, "/var/www/staging.q1tapes.com/deploy"

############################################
# Setup Git
############################################

set :branch, "master"

############################################
# Extra Settings
############################################

#specify extra ssh options:

set :ssh_options, {
    auth_methods: %w(password),
    password: 'koo6iereeL',
    user: 'st_q1tapes',
}
