#!/bin/bash
####################################
# add a submodule to git project   #
# $1 repo                          #
# $2 module                        #
# $3 version : optional            #
# $3 folder  : optional            #
#              default like module #
####################################
addSubModule(){
    if [ -z ${1} ]; then 
       echo "Impossibile aggiungere il modulo se non viene specificato il repository" 
       exit 1 
    else 
      repo=${1}
    fi
    
    if [ -z ${2} ]; then 
       echo "Impossibile aggiungere il modulo se non viene specificato il nome" 
       exit 1 
    else 
      module=${2}
     
    fi
    
    if [ -z ${4} ]; then folder=$module ; else folder=${4} ; fi
   
  
    git submodule add $repo/$module.git content/plugins/$folder
    cd content/plugins/$folder
    if [ -z ${3} ]; then  git checkout ; else  git checkout ${3} ; fi
    cd ../../..
    git add content/plugins/$folder
    git commit -m "aggiunto plugin $module"
    git push origin master
}

######################################################
# remove a submodule from git project                #
# $1 module_name, es: content/advanced-custom-fields #
######################################################
removeSubModule(){  
  if [ -z ${1} ]; then 
     echo "Impossibile rimuovere il modulo se non viene specificato" 
     exit 1 
  else 
    module_name=${1}
  fi
  
  git submodule deinit $module_name    
  git rm $module_name
  git rm --cached $module_name  
  rm -rf .git/modules/$module_name
}

#addSubModule git@gitlab.mondadori.it:digital-innovation fnc-api 1.0.0
#addSubModule git@gitlab.mondadori.it:digital-innovation basic_auth 1.0.0
#addSubModule git@gitlab.mondadori.it:digital-innovation ame-siteorigin-panels 1.0.0
addSubModule git@gitlab.mondadori.it:digital-innovation ame-dfp master

