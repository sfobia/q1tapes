<?php
namespace ElementorPro\Modules\Woocommerce\Widgets;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

abstract class Widget_Base extends \Elementor\Widget_Base {

	public function get_categories() {
		return [ 'woocommerce-elements-single' ];
	}

	public function include_wc_template( $template_name, $args = [] ) {
		if ( ! empty( $args ) && is_array( $args ) ) {
			extract( $args );
		}

		include( WC()->plugin_path() . '/templates/' . $template_name );
	}
}
