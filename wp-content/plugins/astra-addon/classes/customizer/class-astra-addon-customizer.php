<?php
/**
 * Astra Addon Customizer
 *
 * @package Astra Addon
 * @since 1.0.0
 */

if ( ! class_exists( 'Astra_Addon_Customizer' ) ) :

	/**
	 * Astra_Addon_Customizer
	 *
	 * @since 1.0.0
	 */
	class Astra_Addon_Customizer {

		/**
		 * Instance
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator
		 *
		 * @since 1.0.0
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @since 1.4.0
		 */
		public function __construct() {
			add_action( 'customize_register', array( $this, 'customize_register' ) );

			$this->includes();
		}

		/**
		 * Include helper classes for Astra Addon Customizer
		 */
		private function includes() {
			// Astra Customizer Notices.
			require ASTRA_EXT_DIR . 'classes/customizer/class-astra-customizer-notices.php';
		}

		/**
		 * Register custom section and panel.
		 *
		 * @since 1.0.0
		 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
		 */
		function customize_register( $wp_customize ) {
			// Register controls.
			$wp_customize->register_control_type( 'Astra_Control_Background' );

			// Register controls.
			$wp_customize->register_control_type( 'Astra_Control_Border' );

			$wp_customize->register_control_type( 'Astra_Control_Heading' );

			$wp_customize->register_control_type( 'Astra_Control_Responsive_Color' );

			$wp_customize->register_control_type( 'Astra_Control_Responsive_Background' );

			// Helper files.
			require ASTRA_EXT_DIR . 'classes/customizer/controls/background/class-astra-control-background.php';
			require ASTRA_EXT_DIR . 'classes/customizer/controls/border/class-astra-control-border.php';
			require ASTRA_EXT_DIR . 'classes/customizer/controls/responsive-color/class-astra-control-responsive-color.php';
			require ASTRA_EXT_DIR . 'classes/customizer/controls/responsive-background/class-astra-control-responsive-background.php';
			require ASTRA_EXT_DIR . 'classes/customizer/controls/heading/class-astra-control-heading.php';
		}

		/**
		 * Sanitize background obj
		 *
		 * @param  array $bg_obj Background object.
		 * @return array         Background object.
		 */
		static public function sanitize_background_obj( $bg_obj ) {
			if ( is_callable( 'Astra_Customizer_Sanitizes::sanitize_background_obj' ) ) {
				return Astra_Customizer_Sanitizes::sanitize_background_obj( $bg_obj );
			}

			return $bg_obj;
		}

		/**
		 * Sanitize Border
		 *
		 * @param  array $border Background object.
		 * @return array         Background object.
		 */
		static public function sanitize_border( $border ) {
			if ( is_callable( 'Astra_Customizer_Sanitizes::sanitize_border' ) ) {
				return Astra_Customizer_Sanitizes::sanitize_border( $border );
			}

			return $border;
		}

		/**
		 * Sanitize Responsive Background Image
		 *
		 * @param  array $bg_obj Background object.
		 * @return array         Background object.
		 */
		static public function sanitize_responsive_background( $bg_obj ) {
			if ( is_callable( 'Astra_Customizer_Sanitizes::sanitize_responsive_background' ) ) {
				return Astra_Customizer_Sanitizes::sanitize_responsive_background( $bg_obj );
			}

			// Default Responsive Background Image.
			$defaults = array(
				'desktop' => array(
					'background-color'      => '',
					'background-image'      => '',
					'background-repeat'     => 'repeat',
					'background-position'   => 'center center',
					'background-size'       => 'auto',
					'background-attachment' => 'scroll',
				),
				'tablet'  => array(
					'background-color'      => '',
					'background-image'      => '',
					'background-repeat'     => 'repeat',
					'background-position'   => 'center center',
					'background-size'       => 'auto',
					'background-attachment' => 'scroll',
				),
				'mobile'  => array(
					'background-color'      => '',
					'background-image'      => '',
					'background-repeat'     => 'repeat',
					'background-position'   => 'center center',
					'background-size'       => 'auto',
					'background-attachment' => 'scroll',
				),
			);

			// Merge responsive background object and default object into $out_bg_obj array.
			$out_bg_obj = wp_parse_args( $bg_obj, $defaults );

			foreach ( $out_bg_obj as $device => $bg ) {
				foreach ( $bg as $key => $value ) {
					if ( 'background-image' === $key ) {
						$out_bg_obj[ $device ] [ $key ] = esc_url_raw( $value );
					} else {
						$out_bg_obj[ $device ] [ $key ] = esc_attr( $value );
					}
				}
			}
			return $out_bg_obj;
		}

		/**
		 * Sanitize Responsive Color
		 *
		 * @param  array $color_obj color object.
		 * @return array         color object.
		 */
		static public function sanitize_responsive_color( $color_obj ) {

			// Default Responsive Background Image.
			$defaults = array(
				'desktop' => '',
				'tablet'  => '',
				'mobile'  => '',
			);

			// Merge responsive color object and default object into $out_color_obj array.
			$out_color_obj = wp_parse_args( $color_obj, $defaults );

			foreach ( $out_color_obj as $device => $color ) {
				$out_color_obj[ $device ] = Astra_Customizer_Sanitizes::sanitize_alpha_color( $color );
			}
			return $out_color_obj;
		}
	}

	/**
	 * Initialize class object with 'get_instance()' method
	 */
	Astra_Addon_Customizer::get_instance();

endif;
