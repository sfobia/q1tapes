/**
 * This file adds some LIVE to the Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and
 * then make any necessary changes to the page using jQuery.
 *
 * @package Astra Addon
 * @since  1.0.0
 */

( function( $ ) {

	wp.customize( 'astra-settings[site-layout-box-width]', function( value ) {
		value.bind( function( width ) {
			/**
			 * Has sticky header?
			 */
			if ( jQuery( '*[data-stick-maxwidth]' ).length ) {
				jQuery( '*[data-stick-maxwidth]' ).find( '.ast-sticky-active, .ast-header-sticky-active, .ast-custom-footer' ).css( { 'max-width': width + 'px', 'transition': 'none' } );
				jQuery( '*[data-stick-maxwidth]' ).attr( 'data-stick-maxwidth', width );
			}
		} );
	} );

	wp.customize( 'astra-settings[site-layout-box-tb-margin]', function( value ) {
		value.bind( function( margin ) {

			header_top 			= (typeof ( wp.customize._value['astra-settings[above-header-layout]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[above-header-layout]']._value: '';
			header_below		= (typeof ( wp.customize._value['astra-settings[below-header-layout]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[below-header-layout]']._value: '';
			header_above_stick 	= (typeof ( wp.customize._value['astra-settings[header-above-stick]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[header-above-stick]']._value: '';
			header_below_stick 	= (typeof ( wp.customize._value['astra-settings[header-below-stick]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[header-below-stick]']._value: '';
			header_main_stick 	= (typeof ( wp.customize._value['astra-settings[header-main-stick]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[header-main-stick]']._value: '';

			if( header_main_stick || ( header_top != 'disabled' && header_above_stick ) || ( header_below != 'disabled' && header_below_stick ) ) {
				wp.customize.preview.send( 'refresh' );
			}
		} );
	} );

	wp.customize( 'astra-settings[site-layout-padded-pad]', function( value ) {
		value.bind( function( padding ) {

			header_top 			= (typeof ( wp.customize._value['astra-settings[above-header-layout]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[above-header-layout]']._value : '';
			header_below 		= (typeof ( wp.customize._value['astra-settings[below-header-layout]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[below-header-layout]']._value : '';
			header_above_stick 	= (typeof ( wp.customize._value['astra-settings[header-above-stick]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[header-above-stick]']._value : '';
			header_below_stick 	= (typeof ( wp.customize._value['astra-settings[header-below-stick]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[header-below-stick]']._value : '';
			header_main_stick 	= (typeof ( wp.customize._value['astra-settings[header-main-stick]'] ) != 'undefined' ) ? wp.customize._value['astra-settings[header-main-stick]']._value : '';

			if( header_main_stick || ( header_top != 'disabled' && header_above_stick ) || ( header_below != 'disabled' && header_below_stick ) ) {
				wp.customize.preview.send( 'refresh' );
			}
		} );
	} );

	/**
	 * Sticky Header background color opacity
	 */
	wp.customize( 'astra-settings[sticky-header-bg-opc]', function( setting ) {
		setting.bind( function( alpha ) {

	        /**
	         * Colors
	         */
			var desktopHeaderBgColor = '',
			tabletHeaderBgColor = '',
			mobileHeaderBgColor = '',

			desktopPrimaryMenuBgColor = '',
			tabletPrimaryMenuBgColor = '',
			mobilePrimaryMenuBgColor = '',


			desktpTopBgColor 	= '',
			tabletTopBgColor 	= '',
			mobileTopBgColor 	= '',
			
			desktopBelowBgColor 	= '',
			tabletBelowBgColor 	= '',
			mobileBelowBgColor 	= '',
			
			primary_nav 	= '',
			dynamicStyle 	= '';

			/**
			 * Transparent Color Tweak
			 */

			if ( wp.customize._value.hasOwnProperty( "astra-settings[header-bg-obj-responsive]" ) ) {
			 	var header_bg_obj = wp.customize._value['astra-settings[header-bg-obj-responsive]']._value;
				desktopHeaderBgColor = ( undefined !== header_bg_obj['desktop'] ) ? header_bg_obj['desktop']['background-color'] : '';
				tabletHeaderBgColor = ( undefined !== header_bg_obj['tablet'] ) ? header_bg_obj['tablet']['background-color'] : '';
				mobileHeaderBgColor = ( undefined !== header_bg_obj['mobile'] ) ? header_bg_obj['mobile']['background-color'] : '';
			}

			/**
	         * Is Primary Menu BG color
	         */
			if ( wp.customize._value.hasOwnProperty( "astra-settings[primary-menu-bg-obj-responsive]" ) ) {
				primary_menu_bg_color = wp.customize._value['astra-settings[primary-menu-bg-obj-responsive]']._value;
				desktopPrimaryMenuBgColor =  primary_menu_bg_color['desktop']['background-color'];
				tabletPrimaryMenuBgColor =  primary_menu_bg_color['tablet']['background-color'];
				mobilePrimaryMenuBgColor =  primary_menu_bg_color['mobile']['background-color'];
			}	

	        /**
	         * Is Above Header color
	         */
			if ( wp.customize._value.hasOwnProperty( "astra-settings[above-header-bg-obj-responsive]" ) ) {
				var above_header_bg_obj = wp.customize._value['astra-settings[above-header-bg-obj-responsive]']._value;
				desktpTopBgColor = ( undefined !== above_header_bg_obj['desktop'] ) ? above_header_bg_obj['desktop']['background-color'] : '';
				tabletTopBgColor = ( undefined !== above_header_bg_obj['tablet'] ) ? above_header_bg_obj['tablet']['background-color'] : '';
				mobileTopBgColor = ( undefined !== above_header_bg_obj['mobile'] ) ? above_header_bg_obj['mobile']['background-color'] : '';
			}

	        /**
	         * Is Below Header color
	         */
			if ( wp.customize._value.hasOwnProperty( "astra-settings[below-header-bg-obj-responsive]" ) ) {
				sup_bg_obj = wp.customize._value['astra-settings[below-header-bg-obj-responsive]']._value || '';
				desktopBelowBgColor = ( undefined !== sup_bg_obj['desktop'] )? sup_bg_obj['desktop']['background-color'] : '';
				tabletBelowBgColor = ( undefined !== sup_bg_obj['tablet'] )? sup_bg_obj['tablet']['background-color'] : '';
				mobileBelowBgColor = ( undefined !== sup_bg_obj['mobile'] )? sup_bg_obj['mobile']['background-color'] : '';
			}

			/**
	         * Disabled primary nav
	         */
	        if ( wp.customize._value.hasOwnProperty( "astra-settings[disable-primary-nav]" ) ) {
	        	primary_nav = wp.customize._value['astra-settings[disable-primary-nav]']._value;
	        }

			desktopHeaderBgColor = ( desktopHeaderBgColor != '' ) ? desktopHeaderBgColor : '#ffffff';
			tabletHeaderBgColor = ( tabletHeaderBgColor != '' ) ? tabletHeaderBgColor : '#ffffff';
			mobileHeaderBgColor = ( mobileHeaderBgColor != '' ) ? mobileHeaderBgColor : '#ffffff';

			desktpTopBgColor = ( desktpTopBgColor != '' ) ? desktpTopBgColor : '#ffffff';
			tabletTopBgColor = ( tabletTopBgColor != '' ) ? tabletTopBgColor : '';
			mobileTopBgColor = ( mobileTopBgColor != '' ) ? mobileTopBgColor : '';

			desktopBelowBgColor = ( desktopBelowBgColor != '' ) ? desktopBelowBgColor : '#414042';
			tabletBelowBgColor = ( tabletBelowBgColor != '' ) ? tabletBelowBgColor : '';
			mobileBelowBgColor = ( mobileBelowBgColor != '' ) ? mobileBelowBgColor : '';

			/**
			 * Convert colors from HEX to RGBA
			 */
			desktopHeaderBgColor       = astra_hex2rgba( astraRgbaToHex( desktopHeaderBgColor ), alpha );
			tabletHeaderBgColor       = astra_hex2rgba( astraRgbaToHex( tabletHeaderBgColor ), alpha );
			mobileHeaderBgColor       = astra_hex2rgba( astraRgbaToHex( mobileHeaderBgColor ), alpha );

			if ( '' != desktopPrimaryMenuBgColor ) {
				desktopPrimaryMenuBgColor = astra_hex2rgba( astraRgbaToHex( desktopPrimaryMenuBgColor ), alpha );
			}
			if ( '' != tabletPrimaryMenuBgColor ) {
				tabletPrimaryMenuBgColor = astra_hex2rgba( astraRgbaToHex( tabletPrimaryMenuBgColor ), alpha );
			}
			if ( '' != mobilePrimaryMenuBgColor ) {
				mobilePrimaryMenuBgColor = astra_hex2rgba( astraRgbaToHex( mobilePrimaryMenuBgColor ), alpha );
			}
			
			desktpTopBgColor          = astra_hex2rgba( astraRgbaToHex( desktpTopBgColor ), alpha );
			tabletTopBgColor          = astra_hex2rgba( astraRgbaToHex( tabletTopBgColor ), alpha );
			mobileTopBgColor          = astra_hex2rgba( astraRgbaToHex( mobileTopBgColor ), alpha );

			desktopBelowBgColor          = astra_hex2rgba( astraRgbaToHex( desktopBelowBgColor ), alpha );
			tabletBelowBgColor          = astra_hex2rgba( astraRgbaToHex( tabletBelowBgColor ), alpha );
			mobileBelowBgColor          = astra_hex2rgba( astraRgbaToHex( mobileBelowBgColor ), alpha );

			// Main Header.
			var mainHeaderSelector = '.ast-transparent-header #ast-fixed-header .main-header-bar,';
			mainHeaderSelector += '#ast-fixed-header .main-header-bar,';
			mainHeaderSelector += '.ast-transparent-header .main-header-bar.ast-sticky-active,';
			mainHeaderSelector += '.main-header-bar.ast-sticky-active,';
			mainHeaderSelector += '.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar,';
			mainHeaderSelector += '#ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field,';
			mainHeaderSelector += '#ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus';

	        dynamicStyle += mainHeaderSelector + '{ background-color: ' + desktopHeaderBgColor + ';}';
			dynamicStyle += '@media (max-width: 768px) {' + mainHeaderSelector + '{ background-color: ' + tabletHeaderBgColor + ' } }';
			dynamicStyle += '@media (max-width: 544px) {' + mainHeaderSelector + '{ background-color: ' + mobileHeaderBgColor + ';} }';



			if ( '' != desktopPrimaryMenuBgColor ) {
				// Primary Menu Bg Color
				dynamicStyle += '#ast-fixed-header .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point #ast-fixed-header .main-header-menu, #ast-fixed-header .ast-masthead-custom-menu-items{ background-color: ' + desktopPrimaryMenuBgColor + ' }';
				if( primary_nav ) {
					dynamicStyle += ' #ast-fixed-header .main-header-bar .ast-masthead-custom-menu-items, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items { background-color: ' + desktopPrimaryMenuBgColor + ' } ';
				}
			}
			if ( '' != tabletPrimaryMenuBgColor ) {
				// Primary Menu Bg Color
				dynamicStyle += '@media (max-width: 768px) {';
				dynamicStyle += '#ast-fixed-header .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point #ast-fixed-header .main-header-menu, #ast-fixed-header .ast-masthead-custom-menu-items{ background-color: ' + tabletPrimaryMenuBgColor + ' }';
				if( primary_nav ) {
					dynamicStyle += ' #ast-fixed-header .main-header-bar .ast-masthead-custom-menu-items, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items { background-color: ' + tabletPrimaryMenuBgColor + ' } ';
				}
				dynamicStyle += '}';
			}
			if ( '' != mobilePrimaryMenuBgColor ) {
				// Primary Menu Bg Color
				dynamicStyle += '@media (max-width:544px) {';
				dynamicStyle += '#ast-fixed-header .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point #ast-fixed-header .main-header-menu, #ast-fixed-header .ast-masthead-custom-menu-items{ background-color: ' + mobilePrimaryMenuBgColor + ' }';
				if( primary_nav ) {
					dynamicStyle += ' #ast-fixed-header .main-header-bar .ast-masthead-custom-menu-items, .main-header-bar.ast-sticky-active .ast-masthead-custom-menu-items { background-color: ' + mobilePrimaryMenuBgColor + ' } ';
				}
				dynamicStyle += '}';
			}

	        // Above Header.
            var aboveHeaderSelector = '#ast-fixed-header .ast-above-header, .ast-above-header.ast-sticky-active, #ast-fixed-header .ast-above-header .ast-search-menu-icon .search-field, .ast-above-header.ast-sticky-active .ast-search-menu-icon .search-field';
			dynamicStyle += aboveHeaderSelector + '{ background-color: ' + desktpTopBgColor + ';}';
			dynamicStyle += '@media (max-width: 768px) {' + aboveHeaderSelector + '{ background-color: ' + tabletTopBgColor + ' } }';
			dynamicStyle += '@media (max-width: 544px) {' + aboveHeaderSelector + '{ background-color: ' + mobileTopBgColor + ';} }';

	        // Below Header.
	        var belowHeaderSelector = '#ast-fixed-header .ast-below-header, .ast-below-header.ast-sticky-active, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header, #ast-fixed-header .ast-below-header-wrap .ast-search-menu-icon .search-field, .ast-below-header-wrap .ast-sticky-active .ast-search-menu-icon .search-field';
	        dynamicStyle += belowHeaderSelector + '{ background-color: ' + desktopBelowBgColor + ';}';
			dynamicStyle += '@media (max-width: 768px) {' + belowHeaderSelector + '{ background-color: ' + tabletBelowBgColor + ' } }';
			dynamicStyle += '@media (max-width: 544px) {' + belowHeaderSelector + '{ background-color: ' + mobileBelowBgColor + ';} }';


	        /**
	         * Add CSS
	         */
			astra_add_dynamic_css( 'sticky-header-bg-opc', dynamicStyle );

		} );
	} );

	/**
	 * Header background color
	 */
	wp.customize( 'astra-settings[header-bg-obj-responsive]', function( setting ) {
		setting.bind( function( bg_obj ) {

			var desktopHeaderBgColor = ( undefined != bg_obj['desktop'] ) ? bg_obj['desktop']['background-color'] : '',
				tabletHeaderBgColor = ( undefined != bg_obj['tablet'] ) ? bg_obj['tablet']['background-color'] : '',
				mobileHeaderBgColor = ( undefined != bg_obj['mobile'] ) ? bg_obj['mobile']['background-color'] : '';

		    // Colors.
	        var alpha 			= '',
	        	dynamicStyle 	= '';

			// Sticky background color opacity.
			if ( wp.customize._value.hasOwnProperty( "astra-settings[sticky-header-bg-opc]" ) ) {
				alpha = wp.customize._value['astra-settings[sticky-header-bg-opc]']._value;
			}

			desktopHeaderBgColor = ( desktopHeaderBgColor != '' ) ? desktopHeaderBgColor : '#ffffff';
			tabletHeaderBgColor = ( tabletHeaderBgColor != '' ) ? tabletHeaderBgColor : '';
			mobileHeaderBgColor = ( mobileHeaderBgColor != '' ) ? mobileHeaderBgColor : '';

			// Convert colors from HEX to RGBA.
			desktopHeaderBgColor = astra_hex2rgba( astraRgbaToHex( desktopHeaderBgColor ), alpha );
			tabletHeaderBgColor = astra_hex2rgba( astraRgbaToHex( desktopHeaderBgColor ), alpha );
			mobileHeaderBgColor = astra_hex2rgba( astraRgbaToHex( desktopHeaderBgColor ), alpha );

			// Main Header.
			var headerSelector = '.ast-transparent-header #ast-fixed-header .main-header-bar,';
			headerSelector += '#ast-fixed-header .main-header-bar,';
			headerSelector += '.ast-transparent-header .main-header-bar.ast-sticky-active,';
			headerSelector += '.main-header-bar.ast-sticky-active,';
			headerSelector += '.ast-stick-primary-below-wrapper.ast-sticky-active .main-header-bar,';
			headerSelector += '#ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field,';
			headerSelector += '#ast-fixed-header .ast-masthead-custom-menu-items .ast-inline-search .search-field:focus';


			dynamicStyle += headerSelector + '{ background-color: ' + desktopHeaderBgColor + ';}';
			dynamicStyle += '@media (max-width: 768px) {' + headerSelector + '{ background-color: ' + tabletHeaderBgColor + ' } }';
			dynamicStyle += '@media (max-width: 544px) {' + headerSelector + '{ background-color: ' + mobileHeaderBgColor + ';} }';

	        // Add CSS.
			astra_add_dynamic_css( 'sticky-header-bg-color', dynamicStyle );

		} );
	} );

	/**
	 * Primary Menu background color
	 */
	wp.customize( 'astra-settings[primary-menu-bg-obj-responsive]', function( setting ) {
		setting.bind( function( primaryMenuBgColor ) {

	        /**
	         * Colors
	         */
	        var alpha 			= '',
	        	dynamicStyle 	= '',
	        	desktopPrimaryMenuBgColor = '',
				tabletPrimaryMenuBgColor = '',
				mobilePrimaryMenuBgColor = '';

			/**
			 * Sticky background color opacity
			 */
			if ( wp.customize._value.hasOwnProperty( "astra-settings[sticky-header-bg-opc]" ) ) {
				alpha = wp.customize._value['astra-settings[sticky-header-bg-opc]']._value;
			}

			if ( undefined !== primaryMenuBgColor['desktop'] ) {
				desktopPrimaryMenuBgColor = ( primaryMenuBgColor['desktop']['background-color'] != '' ) ? primaryMenuBgColor['desktop']['background-color'] : '#ffffff';
			}
			if ( undefined !== primaryMenuBgColor['tablet'] ) {
				tabletPrimaryMenuBgColor = ( primaryMenuBgColor['tablet']['background-color'] != '' ) ? primaryMenuBgColor['tablet']['background-color'] : '#ffffff';
			}
			if ( undefined !== primaryMenuBgColor['mobile'] ) {
				mobilePrimaryMenuBgColor = ( primaryMenuBgColor['mobile']['background-color'] != '' ) ? primaryMenuBgColor['mobile']['background-color'] : '#ffffff';
			}


			/**
			 * Convert colors from HEX to RGBA
			 */
			desktopPrimaryMenuBgColor = astra_hex2rgba( astraRgbaToHex( desktopPrimaryMenuBgColor ), alpha );
			tabletPrimaryMenuBgColor = astra_hex2rgba( astraRgbaToHex( tabletPrimaryMenuBgColor ), alpha );
			mobilePrimaryMenuBgColor = astra_hex2rgba( astraRgbaToHex( mobilePrimaryMenuBgColor ), alpha );

			// Main Header.
			var headerSelector = '#ast-fixed-header .main-header-bar .main-header-menu, .main-header-bar.ast-sticky-active .main-header-menu, .ast-header-break-point #ast-fixed-header .main-header-menu, #ast-fixed-header .ast-masthead-custom-menu-items';

			dynamicStyle += headerSelector + '{ background-color: ' + desktopPrimaryMenuBgColor + ';}';
			dynamicStyle += '@media (max-width: 768px) {' + headerSelector + '{ background-color: ' + tabletPrimaryMenuBgColor + ' } }';
			dynamicStyle += '@media (max-width: 544px) {' + headerSelector + '{ background-color: ' + mobilePrimaryMenuBgColor + ';} }';

	        /**
	         * Add CSS
	         */
			astra_add_dynamic_css( 'sticky-primary-menu-bg-color', dynamicStyle );

		} );
	} );

	/**
	 * Sticky Above Header background color opacity
	 */
	wp.customize( 'astra-settings[above-header-bg-obj-responsive]', function( setting ) {
		setting.bind( function( top_bg_obj ) {
	
			var desktopAboveBgColor = ( undefined !== top_bg_obj['desktop'] ) ? top_bg_obj['desktop']['background-color'] : '#ffffff',
			tabletAboveBgColor = ( undefined !== top_bg_obj['tablet'] ) ? top_bg_obj['tablet']['background-color'] : '',
			mobileAboveBgColor = ( undefined !== top_bg_obj['mobile'] ) ? top_bg_obj['mobile']['background-color'] : '';

	        // Colors.
	        var alpha = '',
	        	dynamicStyle 	= '';

			// Sticky background color opacity.
			if ( wp.customize._value.hasOwnProperty( "astra-settings[sticky-header-bg-opc]" ) ) {
				alpha = wp.customize._value['astra-settings[sticky-header-bg-opc]']._value;
			}

			// Convert colors from HEX to RGBA.
			desktopAboveBgColor    = astra_hex2rgba( astraRgbaToHex( desktopAboveBgColor ), alpha );
			tabletAboveBgColor    = astra_hex2rgba( astraRgbaToHex( tabletAboveBgColor ), alpha );
			mobileAboveBgColor    = astra_hex2rgba( astraRgbaToHex( mobileAboveBgColor ), alpha );

	        // Above Header.
	        if ( wp.customize._value.hasOwnProperty( "astra-settings[above-header-bg-obj-responsive]" ) ) {
	            var aboveHeaderSelector = '#ast-fixed-header .ast-above-header, .ast-above-header.ast-sticky-active,#ast-fixed-header .ast-above-header .ast-search-menu-icon .search-field, .ast-above-header.ast-sticky-active .ast-search-menu-icon .search-field';

	            dynamicStyle += aboveHeaderSelector + '{ background-color: ' + desktopAboveBgColor + ';}';
				dynamicStyle += '@media (max-width: 768px) {' + aboveHeaderSelector + '{ background-color: ' + tabletAboveBgColor + ' } }';
				dynamicStyle += '@media (max-width: 544px) {' + aboveHeaderSelector + '{ background-color: ' + mobileAboveBgColor + ';} }';
	        }

	        // Add CSS.
			astra_add_dynamic_css( 'sticky-above-header-bg-color', dynamicStyle );

		} );
	} );

	/**
	 * Sticky Above Header background color opacity
	 */
	wp.customize( 'astra-settings[below-header-bg-obj-responsive]', function( setting ) {
		setting.bind( function( sup_bg_obj ) {

			var desktopBelowBgColor = ( undefined !== sup_bg_obj['desktop'] ) ? sup_bg_obj['desktop']['background-color'] : '',
			tabletBelowBgColor = ( undefined !== sup_bg_obj['tablet'] ) ? sup_bg_obj['tablet']['background-color'] : '',
			mobileBelowBgColor = ( undefined !== sup_bg_obj['mobile'] ) ? sup_bg_obj['mobile']['background-color'] : '';

	        /**
	         * Colors
	         */
	        var alpha = '',
	        	dynamicStyle 	= '';

			/**
			 * Sticky background color opacity
			 */

			if ( wp.customize._value.hasOwnProperty( "astra-settings[sticky-header-bg-opc]" ) ) {
				alpha = wp.customize._value['astra-settings[sticky-header-bg-opc]']._value;
			}

			desktopBelowBgColor = ( desktopBelowBgColor != '' ) ? desktopBelowBgColor : '#414042';
			tabletBelowBgColor = ( tabletBelowBgColor != '' ) ? tabletBelowBgColor : '';
			mobileBelowBgColor = ( mobileBelowBgColor != '' ) ? mobileBelowBgColor : '';

			/**
			 * Convert colors from HEX to RGBA
			 */
			desktopBelowBgColor    = astra_hex2rgba( astraRgbaToHex( desktopBelowBgColor ), alpha );

	        // Below Header.
	        if ( wp.customize._value.hasOwnProperty( "astra-settings[below-header-bg-obj-responsive]" ) ) {
	            var belowHeaderSelector = '#ast-fixed-header .ast-below-header, .ast-below-header.ast-sticky-active, .ast-stick-primary-below-wrapper.ast-sticky-active .ast-below-header, #ast-fixed-header .ast-below-header-wrap .ast-search-menu-icon .search-field, .ast-below-header-wrap .ast-sticky-active .ast-search-menu-icon .search-field';

	            dynamicStyle += belowHeaderSelector + '{ background-color: ' + desktopBelowBgColor + ';}';
				dynamicStyle += '@media (max-width: 768px) {' + belowHeaderSelector + '{ background-color: ' + tabletBelowBgColor + ' } }';
				dynamicStyle += '@media (max-width: 544px) {' + belowHeaderSelector + '{ background-color: ' + mobileBelowBgColor + ';} }';
	        }

	        /**
	         * Add CSS
	         */
			astra_add_dynamic_css( 'sticky-below-header-bg-color', dynamicStyle );

		} );
	} );
	
	/**
	 * Sticky Above Header background color opacity
	 */
	wp.customize( 'astra-settings[sticky-header-logo-width]', function( setting ) {
		setting.bind( function( logo_width ) {
			if ( logo_width['desktop'] != '' || logo_width['tablet'] != '' || logo_width['mobile'] != '' ) {
				var dynamicStyle = '.site-logo-img .sticky-custom-logo img {max-width: ' + logo_width['desktop'] + 'px;} #masthead .site-logo-img .sticky-custom-logo .astra-logo-svg, .site-logo-img .sticky-custom-logo .astra-logo-svg, .ast-sticky-main-shrink .ast-sticky-shrunk .site-logo-img .astra-logo-svg { width: ' + logo_width['desktop'] + 'px;} @media( max-width: 768px ) { .site-logo-img .sticky-custom-logo img {max-width: ' + logo_width['tablet'] + 'px;} #masthead .site-logo-img .sticky-custom-logo .astra-logo-svg, .site-logo-img .sticky-custom-logo .astra-logo-svg, .ast-sticky-main-shrink .ast-sticky-shrunk .site-logo-img .astra-logo-svg { width: ' + logo_width['tablet'] + 'px;} } @media( max-width: 544px ) { .site-logo-img .sticky-custom-logo img {max-width: ' + logo_width['mobile'] + 'px;} #masthead .site-logo-img .sticky-custom-logo .astra-logo-svg, .site-logo-img .sticky-custom-logo .astra-logo-svg, .ast-sticky-main-shrink .ast-sticky-shrunk .site-logo-img .astra-logo-svg { width: ' + logo_width['mobile'] + 'px;} }'
				astra_add_dynamic_css( 'sticky-header-logo-width', dynamicStyle );
			}
			else{
				wp.customize.preview.send( 'refresh' );
			}
		});
	});

} )( jQuery );
