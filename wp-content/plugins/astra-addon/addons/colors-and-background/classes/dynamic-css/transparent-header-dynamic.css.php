<?php
/**
 * Colors & Background - Dynamic CSS
 *
 * @package Astra Addon
 */

add_filter( 'astra_dynamic_css', 'astra_ext_transparent_header_colors_dynamic_css' );

/**
 * Dynamic CSS
 *
 * @param  string $dynamic_css          Astra Dynamic CSS.
 * @param  string $dynamic_css_filtered Astra Dynamic CSS Filters.
 * @return string
 */
function astra_ext_transparent_header_colors_dynamic_css( $dynamic_css, $dynamic_css_filtered = '' ) {

	$transparent_bg_color           = astra_get_option( 'transparent-header-bg-color' );
	$transparent_color_site_title   = astra_get_option( 'transparent-header-color-site-title' );
	$transparent_color_h_site_title = astra_get_option( 'transparent-header-color-h-site-title' );
	$transparent_menu_bg_color      = astra_get_option( 'transparent-menu-bg-color' );
	$transparent_menu_color         = astra_get_option( 'transparent-menu-color' );
	$transparent_menu_h_color       = astra_get_option( 'transparent-menu-h-color' );

	$header_break_point = astra_header_break_point(); // Header Break Point.

	$parse_css = '';

	/**
	 * Transparent Header
	 */
	$transparent_header = array(

		'.ast-theme-transparent-header .main-header-bar, .ast-theme-transparent-header .ast-header-break-point .main-header-menu' => array(
			'background-color' => esc_attr( $transparent_bg_color ),
		),
		'.ast-theme-transparent-header .main-header-bar .ast-search-menu-icon form' => array(
			'background-color' => esc_attr( $transparent_bg_color ),
		),
		'.ast-theme-transparent-header .ast-masthead-custom-menu-items .slide-search .search-field' => array(
			'background-color' => esc_attr( $transparent_bg_color ),
		),
		'.ast-theme-transparent-header .ast-masthead-custom-menu-items .slide-search .search-field:focus' => array(
			'background-color' => esc_attr( $transparent_bg_color ),
		),

		'.ast-theme-transparent-header .ast-above-header, .ast-theme-transparent-header .ast-below-header' => array(
			'background-color' => esc_attr( $transparent_bg_color ),
		),

		'.ast-theme-transparent-header .site-title a, .ast-theme-transparent-header .site-title a:focus, .ast-theme-transparent-header .site-title a:hover, .ast-theme-transparent-header .site-title a:visited' => array(
			'color' => esc_attr( $transparent_color_site_title ),
		),
		'.ast-theme-transparent-header .site-header .site-title a:hover' => array(
			'color' => esc_attr( $transparent_color_h_site_title ),
		),

		'.ast-theme-transparent-header .site-header .site-description' => array(
			'color' => esc_attr( $transparent_color_site_title ),
		),

		'.ast-theme-transparent-header .main-header-menu, .ast-theme-transparent-header .ast-header-break-point .main-header-menu' => array(
			'background-color' => esc_attr( $transparent_menu_bg_color ),
		),
	);

	// Only apply colors for above poing screens.
	$transparent_above_break_point_colors = array(
		'.ast-theme-transparent-header .main-header-menu li.current-menu-item > a, .ast-theme-transparent-header .main-header-menu li.current-menu-ancestor > a, .ast-theme-transparent-header .main-header-menu li.current_page_item > a' => array(
			'color' => esc_attr( $transparent_menu_h_color ),
		),
		'.ast-theme-transparent-header .main-header-menu a:hover, .ast-theme-transparent-header .main-header-menu li:hover > a, .ast-theme-transparent-header .main-header-menu li.focus > a' => array(
			'color' => esc_attr( $transparent_menu_h_color ),
		),
		'.ast-theme-transparent-header .main-header-menu .ast-masthead-custom-menu-items a:hover, .ast-theme-transparent-header .main-header-menu li:hover > .ast-menu-toggle, .ast-theme-transparent-header .main-header-menu li.focus > .ast-menu-toggle' => array(
			'color' => esc_attr( $transparent_menu_h_color ),
		),

		'.ast-theme-transparent-header .main-header-menu, .ast-theme-transparent-header .main-header-menu a, .ast-theme-transparent-header .ast-masthead-custom-menu-items .slide-search .search-submit, .ast-theme-transparent-header .ast-masthead-custom-menu-items, .ast-theme-transparent-header .ast-masthead-custom-menu-items a' => array(
			'color' => esc_attr( $transparent_menu_color ),
		),
	);
	/* Parse CSS from array() */
	$parse_css .= astra_parse_css( $transparent_header );

	if ( '' == $transparent_menu_bg_color ) {
		// If Menu bg color for transparent header is default(applied from theme) then add default color,link color to transparent header navigation.
		$parse_css .= astra_parse_css( $transparent_above_break_point_colors, $header_break_point );
	} else {
		// If Menu bg color for transparent header is updated then add selected color,link color to transparent header navigation from customizer.
		$parse_css .= astra_parse_css( $transparent_above_break_point_colors );
	}

	return $dynamic_css . $parse_css;
}
