/**
 * Showing and Hiding controls of Customizer.
 *
 * @package Astra Addon
 * @since 1.0.0
 */

( function( $ ) {
	ASTControlTrigger.addHook( 'astra-toggle-control', function( argument, api ) {

		/* Layout select */
		ASTCustomizerToggles ['astra-settings[different-transparent-logo]'] = [

			{
				controls: [
					'astra-settings[transparent-header-logo]',
					'astra-settings[transparent-header-retina-logo]',
					'astra-settings[transparent-header-logo-width]'
				],
				callback: function( val ) {

					transparent_logo = (typeof api( 'astra-settings[transparent-header-enable]' ) != 'undefined') ? api( 'astra-settings[transparent-header-enable]' ).get() : '';

					if ( val && transparent_logo ) {
						return true;
					}

					return false;
				}
			}
		];

		ASTCustomizerToggles ['astra-settings[different-transparent-logo]'] = [
			{
				controls: [
					'astra-settings[transparent-header-logo]',
					'astra-settings[transparent-header-retina-logo]',
					'astra-settings[transparent-header-logo-width]',
				],
				callback: function( val ) {

					if ( val ) {
						return true;
					}

					return false;
				}
			},
		]

	});
})( jQuery );
