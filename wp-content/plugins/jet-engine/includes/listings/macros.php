<?php
/**
 * Class description
 *
 * @package   package_name
 * @author    Cherry Team
 * @license   GPL-2.0+
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Jet_Engine_Listings_Macros' ) ) {

	/**
	 * Define Jet_Engine_Listings_Macros class
	 */
	class Jet_Engine_Listings_Macros {

		/**
		 * Return available macros list
		 *
		 * @return [type] [description]
		 */
		public function get_all() {
			return apply_filters( 'jet-theme-core/listings/macros-list', array(
				'title'       => array( $this, 'get_title' ),
				'field_value' => array( $this, 'get_field_value' ),
			) );
		}

		/**
		 * Return verbosed macros list
		 *
		 * @return [type] [description]
		 */
		public function verbose_macros_list() {

			$macros = $this->get_all();
			$result = '';
			$sep    = '';

			foreach ( $macros as $key => $data ) {
				$result .= $sep . '%' . $key . '%';
				$sep     = ', ';
			}

			return $result;

		}

		/**
		 * Get current object title
		 *
		 * @return [type] [description]
		 */
		public function get_title( $field_value = null ) {

			$object = jet_engine()->listings->data->get_current_object();
			$class  = get_class( $object );
			$result = '';

			switch ( $class ) {
				case 'WP_Post':
					$result = $object->post_title;
					break;

				case 'WP_Term':
					$result = $object->name;
					break;
			}

			return $result;

		}

		/**
		 * Returns current field value
		 *
		 * @param  [type] $field_value [description]
		 * @return [type]              [description]
		 */
		public function get_field_value( $field_value = null ) {
			return $field_value;
		}

		/**
		 * Do macros inside string
		 *
		 * @param  [type] $string [description]
		 * @return [type]         [description]
		 */
		public function do_macros( $string, $field_value ) {

			$macros = $this->get_all();

			return preg_replace_callback( '/%([a-z_-]+)%/', function( $matches ) use ( $macros, $field_value ) {

				$found = $matches[1];

				if ( ! isset( $macros[ $found ] ) ) {
					return $matches[0];
				}

				$cb = $macros[ $found ];

				if ( ! is_callable( $cb ) ) {
					return $matches[0];
				}

				return call_user_func( $cb, $field_value );

			}, $string );

		}

	}

}
