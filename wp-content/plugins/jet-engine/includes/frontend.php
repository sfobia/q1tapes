<?php
/**
 * Class description
 *
 * @package   package_name
 * @author    Cherry Team
 * @license   GPL-2.0+
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Jet_Engine_Frontend' ) ) {

	/**
	 * Define Jet_Engine_Frontend class
	 */
	class Jet_Engine_Frontend {

		private $listing_id = null;

		public function set_listing( $listing_id = null ) {
			$this->listing_id = $listing_id;
		}

		public function reset_listing() {
			$this->reset_data();
			$this->listing_id = null;
		}

		public function get_listing_item( $post ) {

			$this->setup_data( $post );
			$listing_id = $this->listing_id;

			return Elementor\Plugin::instance()->frontend->get_builder_content_for_display( $listing_id );
		}

		/**
		 * Setup data
		 *
		 * @param  [type] $post [description]
		 * @return [type]       [description]
		 */
		public function setup_data( $post_obj = null ) {

			if ( 'posts' === jet_engine()->listings->data->get_listing_source() ) {
				global $post;
				$post = $post_obj;
				setup_postdata( $post );
			}

			jet_engine()->listings->data->set_current_object( $post_obj );
		}

		/**
		 * Reset data
		 * @return [type] [description]
		 */
		public function reset_data() {
			if ( 'posts' === jet_engine()->listings->data->get_listing_source() ) {
				wp_reset_postdata();
			}

			jet_engine()->listings->data->set_current_object( false );
		}

	}

}
