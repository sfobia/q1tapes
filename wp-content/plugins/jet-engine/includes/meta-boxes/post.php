<?php
/**
 * Meta oxes mamager
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Jet_Engine_CPT_Meta' ) ) {

	/**
	 * Define Jet_Engine_CPT_Meta class
	 */
	class Jet_Engine_CPT_Meta {

		public static $index = 0;

		/**
		 * Constructor for the class
		 */
		function __construct( $post_type, $meta_box ) {

			new Cherry_X_Post_Meta( array(
				'id'            => $this->get_box_id(),
				'title'         => esc_html__( 'Settings', 'jet-engine' ),
				'page'          => array( $post_type ),
				'context'       => 'normal',
				'priority'      => 'high',
				'callback_args' => false,
				'builder_cb'    => array( $this, 'get_builder_for_meta' ),
				'fields'        => $this->prepare_meta_fields( $meta_box ),
			) );

		}

		public function get_box_id() {
			self::$index++;
			return 'jet-engine-cpt-' . self::$index;
		}

		/**
		 * Returns builder for meta
		 *
		 * @return [type] [description]
		 */
		public function get_builder_for_meta() {

			$builder_data = jet_engine()->framework->get_included_module_data( 'cherry-x-interface-builder.php' );

			return new CX_Interface_Builder(
				array(
					'path' => $builder_data['path'],
					'url'  => $builder_data['url'],
				)
			);

		}

		/**
		 * Prepare meta fields for registering
		 *
		 * @param  array  $meta_box [description]
		 * @return [type]           [description]
		 */
		public function prepare_meta_fields( $meta_box = array() ) {

			$result = array();

			foreach ( $meta_box as $field ) {

				$result[ $field['name'] ] = array(
					'type'    => $field['type'],
					'element' => 'control',
					'title'   => $field['title'],
				);

				switch ( $field['type'] ) {
					case 'select':

						$result[ $field['name'] ]['options'] = $this->prepare_select_options(
							$field['options']
						);

						break;

					case 'radio':

						$result[ $field['name'] ]['options'] = $this->prepare_radio_options(
							$field['options']
						);

						break;

					case 'repeater':

						$result[ $field['name'] ]['fields'] = $this->prepare_repeater_fields(
							$field['repeater-fields']
						);

						break;

					case 'iconpicker':

						$result[ $field['name'] ]['icon_data'] = $this->get_icon_data();

						break;

					case 'media':

						$result[ $field['name'] ]['multi_upload'] = false;

						break;

					case 'date':
					case 'time':
					case 'datetime':

						$result[ $field['name'] ]['type']       = 'text';
						$result[ $field['name'] ]['input_type'] = $field['type'];

						if ( ! empty( $field['is_timestamp'] ) && 'true' === $field['is_timestamp'] ) {
							$result[ $field['name'] ]['is_timestamp'] = true;
						}

						break;

				}

			}

			return $result;

		}

		/**
		 * Returns default icon data
		 *
		 * @return void
		 */
		public function get_icon_data() {

			ob_start();

			include jet_engine()->plugin_path( 'assets/js/icons.json' );
			$json = ob_get_clean();

			$icons_list = array();
			$icons      = json_decode( $json, true );

			foreach ( $icons['icons'] as $icon ) {
				$icons_list[] = $icon['id'];
			}

			return array(
				'icon_set'    => 'jetFontAwesome',
				'icon_css'    => ELEMENTOR_ASSETS_URL . 'lib/font-awesome/css/font-awesome.min.css',
				'icon_base'   => 'fa',
				'icon_prefix' => 'fa-',
				'icons'       => $icons_list,
			);

		}

		public function prepare_repeater_fields( $repeater_fields = array() ) {

			if ( ! $repeater_fields ) {
				$repeater_fields = array();
			}

			$result = array();

			foreach ( $repeater_fields as $field ) {
				$result[ $field['name'] ] = array(
					'type'  => $field['type'],
					'id'    => $field['name'],
					'name'  => $field['name'],
					'label' => $field['title'],
				);

				if ( 'iconpicker' === $field['type'] ) {
					$result[ $field['name'] ]['icon_data'] = $this->get_icon_data();
				}

			}

			return $result;

		}

		public function prepare_radio_options( $options = array() ) {

			if ( ! $options ) {
				$options = array();
			}

			$result = array();

			foreach ( $options as $option ) {
				$result[ $option['key'] ] = array(
					'label' => $option['value']
				);
			}

			return $result;

		}

		/**
		 * Prepare options for select
		 * @return [type] [description]
		 */
		public function prepare_select_options( $options = array() ) {

			if ( ! $options ) {
				$options = array();
			}

			$result = array();

			foreach ( $options as $option ) {
				$result[ $option['key'] ] = $option['value'];
			}

			return $result;
		}

	}

}
