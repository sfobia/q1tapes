<?php
/**
 * Mobile menu trigger template
 */
?>
<div class="jet-nav__mobile-trigger jet-nav-mobile-trigger-align-<?php echo $trigger_align; ?>">
	<i class="fa fa-bars jet-nav__mobile-trigger-open" aria-hidden="true"></i>
	<i class="fa fa-times jet-nav__mobile-trigger-close" aria-hidden="true"></i>
</div>