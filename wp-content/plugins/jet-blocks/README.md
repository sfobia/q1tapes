# JetBlocks For Elementor

Enjoy the easy-to-use widgets made for enriching headers & footers with content.

# ChangeLog

## [1.1.0](https://github.com/ZemezLab/jet-blocks/releases/tag/1.1.0)

* Added: new Breadcrumbs widget
* Added: the full-screen layout for Search widget
* Added: the improved sticky section functionality
* Added: multiple performance improvements and bug fixes
* Added: dummy data

## [1.0.0](https://github.com/ZemezLab/jet-blocks/releases/tag/1.0.0)

* Init
