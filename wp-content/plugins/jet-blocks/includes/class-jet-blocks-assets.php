<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Jet_Blocks_Assets' ) ) {

	/**
	 * Define Jet_Blocks_Assets class
	 */
	class Jet_Blocks_Assets {

		/**
		 * A reference to an instance of this class.
		 *
		 * @since 1.0.0
		 * @var   object
		 */
		private static $instance = null;

		/**
		 * Constructor for the class
		 */
		public function init() {

			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
			add_action( 'admin_enqueue_scripts',  array( $this, 'enqueue_admin_styles' ) );

			add_action( 'elementor/frontend/before_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			add_action( 'elementor/editor/before_enqueue_scripts', array( $this, 'editor_scripts' ) );

			add_action( 'elementor/editor/after_enqueue_styles', array( $this, 'editor_styles' ) );
		}

		/**
		 * Enqueue public-facing stylesheets.
		 *
		 * @since 1.0.0
		 * @access public
		 * @return void
		 */
		public function enqueue_styles() {

			wp_enqueue_style(
				'jet-blocks',
				jet_blocks()->plugin_url( 'assets/css/jet-blocks.css' ),
				false,
				jet_blocks()->get_version()
			);

		}

		/**
		 * Enqueue admin styles
		 *
		 * @return void
		 */
		public function enqueue_admin_styles() {
			$screen = get_current_screen();

			// Jet setting page check
			if ( 'elementor_page_jet-blocks-settings' === $screen->base ) {
				wp_enqueue_style(
					'jet-blocks-admin-css',
					jet_blocks()->plugin_url( 'assets/css/jet-blocks-admin.css' ),
					false,
					jet_blocks()->get_version()
				);
			}
		}

		/**
		 * Enqueue plugin scripts only with elementor scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {

			wp_enqueue_script(
				'jet-blocks',
				jet_blocks()->plugin_url( 'assets/js/jet-blocks.js' ),
				array( 'jquery', 'elementor-frontend' ),
				jet_blocks()->get_version(),
				true
			);

			wp_localize_script(
				'jet-blocks',
				'jetBlocksData',
				apply_filters( 'jet-blocks/frontend/localize-data', array() )
			);

		}

		/**
		 * Enqueue elemnetor editor-related styles
		 *
		 * @return void
		 */
		public function editor_styles() {

			wp_enqueue_style(
				'jet-blocks-editor',
				jet_blocks()->plugin_url( 'assets/css/jet-blocks-editor.css' ),
				array(),
				jet_blocks()->get_version()
			);

		}

		/**
		 * Enqueue plugin scripts only with elementor scripts
		 *
		 * @return void
		 */
		public function editor_scripts() {

			wp_enqueue_script(
				'jet-blocks-editor',
				jet_blocks()->plugin_url( 'assets/js/jet-blocks-editor.js' ),
				array( 'jquery' ),
				jet_blocks()->get_version(),
				true
			);

		}

		/**
		 * Returns the instance.
		 *
		 * @since  1.0.0
		 * @return object
		 */
		public static function get_instance() {

			// If the single instance hasn't been set, set it now.
			if ( null == self::$instance ) {
				self::$instance = new self;
			}
			return self::$instance;
		}
	}

}

/**
 * Returns instance of Jet_Blocks_Assets
 *
 * @return object
 */
function jet_blocks_assets() {
	return Jet_Blocks_Assets::get_instance();
}
