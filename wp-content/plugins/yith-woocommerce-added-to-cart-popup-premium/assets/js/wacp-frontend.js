/**
 * wacp-frontend.js
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Added to Cart Popup
 * @version 1.2.1
 */

jQuery(document).ready(function($) {
    "use strict";

    if( typeof yith_wacp == 'undefined' )
        return;

    var PS_instance,
        xhr,
        popup       = $('#yith-wacp-popup'),
        overlay     = popup.find( '.yith-wacp-overlay'),
        close       = popup.find( '.yith-wacp-close'),
        close_popup = function(){
            // remove class to html
            $('html').removeClass( 'yith_wacp_open' );
            // remove class open
            popup.removeClass( 'open' );
            // after 2 sec remove content
            setTimeout(function () {
                PS_instance.destroy();
            }, 1000);

            $(document).trigger( 'yith_wacp_popup_after_closing' );
        },
        // center popup function
        center_popup    = function() {
            var t = popup.find( '.yith-wacp-wrapper'),
                window_w = $(window).width(),
                window_h = $(window).height(),
                width    = ( ( window_w - 60 ) > yith_wacp.popup_size.width ) ? yith_wacp.popup_size.width : ( window_w - 60 ),
                height   = ( ( window_h - 120 ) > yith_wacp.popup_size.height ) ? yith_wacp.popup_size.height : ( window_h - 120 );

            t.css({
                'left' : (( window_w/2 ) - ( width/2 )),
                'top' : (( window_h/2 ) - ( height/2 )),
                'width'     : width + 'px',
                'height'    : height + 'px'
            });
        },
        // function that handle the popup opening
        handle_popup_open   = function( data ) {
            // add content
            var popup_content = popup.find('.yith-wacp-content');
            popup_content.html( data );

            // check if popup is still open, if yes, update it.
            if( popup.hasClass('open') ) {

                // update scroll
                if( typeof PerfectScrollbar != 'undefined' ) {
                    PS_instance.update();
                }
                // then scroll to Top
                popup_content.scrollTop(0);
                $(document).trigger( 'yith_wacp_popup_changed', [ popup ] );
            }
            else {
                $(document).trigger( 'yith_wacp_popup_before_opening', [ popup ] );
                // position popup
                center_popup();
                //scroll
                if( typeof PerfectScrollbar != 'undefined' ) {
                    PS_instance = new PerfectScrollbar( '.yith-wacp-content', {
                        suppressScrollX : true
                    });
                }

                if( yith_wacp.is_mobile ) {
                    // add class to html for prevent page scroll on mobile device
                    $('html').addClass( 'yith_wacp_open' );
                }
                popup.addClass('open');

                $(document).trigger( 'yith_wacp_popup_after_opening', [ popup ] );
            }

            return false;
        },
        // function to get param from url
        getUrlParameter     = function getUrlParameter( sURL, sParam ) {
            var sURLVariables = sURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        },
        waiting_ajax       = function( elem ) {
            elem.block({
                message   : null,
                overlayCSS: {
                    background: '#fff url(' + yith_wacp.loader + ') no-repeat center',
                    opacity   : 0.5,
                    cursor    : 'none'
                }
            });
        };

    // ADD TO CART LOOP AND SINGLE PRODUCT
    $('body').on( 'added_to_cart wacp_single_added_to_cart cleverswatch_after_add_to_cart', function( ev, fragmentsJSON, cart_hash, button ){

        if( typeof fragmentsJSON == 'undefined' )
            fragmentsJSON = $.parseJSON( sessionStorage.getItem( wc_cart_fragments_params.fragment_name ) );

        if( typeof fragmentsJSON.yith_wacp_message != 'undefined' )
            handle_popup_open( fragmentsJSON.yith_wacp_message );
    });
    // REQUEST A QUOTE
    $(document).on( 'yith_wwraq_added_successfully', function( ev, response ) {
        if( typeof response.yith_wacp_raq != 'undefined' )
            handle_popup_open( response.yith_wacp_raq );
    });

    // ACTIONS
    // remove from cart ajax
    popup.on( 'click', '.yith-wacp-remove-cart, .item-remove a.remove', function(ev) {
        ev.preventDefault();

        var t     = $(this),
            item_key = t.data('item_key') ? t.data('item_key') : getUrlParameter( t.attr('href'), 'remove_item' ),
            data = {
                action: yith_wacp.actionremove,
                item_key: item_key,
                context: 'frontend'
            };

        waiting_ajax( t.parents('table') );

        $.ajax({
            url: yith_wacp.ajaxurl.toString().replace( '%%endpoint%%', yith_wacp.actionremove ),
            data: data,
            dataType: 'html',
            success: function( res ) {

                if( res != '' ) {
                    popup.find('.yith-wacp-content').html( res );

                    $(document).trigger( 'yith_wacp_popup_changed', [ popup ] );
                }
                else {
                    close_popup();
                }
            }
        });
    });
    // update from cart ajax
    popup.on( 'change', 'table.cart-list input.qty', function(ev) {
        ev.preventDefault();

        var t     = $(this),
            data = {
                action: yith_wacp.actionupdate,
                item_key: t.attr('name').replace( /\[|\]|qty/gi, ''),
                qty: t.val(),
                context: 'frontend'
            };

        waiting_ajax( t.parents('table') );

        $.ajax({
            url: yith_wacp.ajaxurl.toString().replace( '%%endpoint%%', yith_wacp.actionupdate ),
            data: data,
            dataType: 'html',
            success: function( res ) {
                if( res != '' ) {
                    popup.find('.yith-wacp-content').html( res );

                    $(document).trigger( 'yith_wacp_popup_changed', [ popup ] );

                    $( document.body ).trigger( 'wc_fragment_refresh' );
                }
            }
        });
    });
    // continue shopping
    popup.on( 'click', 'a.continue-shopping', function (e) {
        if( $(this).attr('href') != '#' ) {
            return;
        }
        e.preventDefault();
        close_popup();
    });
    // update raq quote list
    popup.on( 'submit', '#yith-ywraq-form', function(ev){
        ev.preventDefault();

        var t    = $(this),
            form = t.serializeArray();

        // add action
        form.push({ name: "action", value: yith_wacp.actionupdateraq }, { name: "context", value: "frontend" } );

        $.ajax({
            url: yith_wacp.ajaxurl.toString().replace( '%%endpoint%%', yith_wacp.actionupdateraq ),
            data: $.param( form ),
            dataType: 'json',
            type: 'POST',
            success: function( res ) {
                handle_popup_open( res.yith_wacp_raq );
            }
        });
    });


    // GENERAL ACTION
    overlay.on( 'click', close_popup );
    close.on( 'click', function(ev){
        ev.preventDefault();
        close_popup();
    });

    $( window ).on( 'resize yith_wacp_popup_changed', center_popup );

    /*######################################
     ADD TO CART AJAX IN SINGLE PRODUCT PAGE
    ########################################*/

    $(document).on( 'submit', yith_wacp.form_selectors, function( ev ) {
        if( typeof wc_cart_fragments_params === 'undefined' || ! yith_wacp.enable_single || $(this).parents('.product-type-external').length === 1 ) {
            return;
        }

        var $supports_html5_storage,
            $private        = false,
            form            = $(this),
            button          = form.find( 'button[type="submit"]'),
            exclude         = form.find( 'input[name="yith-wacp-is-excluded"]' ),
            is_one_click    = form.find('input[name="_yith_wocc_one_click"]').val() == 'is_one_click',
            data;

        try {
            $supports_html5_storage = ( 'sessionStorage' in window && window.sessionStorage !== null );

            window.sessionStorage.setItem( 'wc', 'test' );
            window.sessionStorage.removeItem( 'wc' );
        } catch( err ) {
            $supports_html5_storage = false;
            if ( err.code == DOMException.QUOTA_EXCEEDED_ERR && window.sessionStorage.length == 0) {
                $private = true
            }
        }


        // check if excluded
        if( exclude.length || $private || is_one_click )
            return;

        ev.preventDefault();

        $(document).trigger( 'yith_wacp_adding_cart_single' );

        button.addClass('loading')
            .removeClass('added');

        // Process Form
        var dataForm = new FormData();
        $.each( form.find( "input[type='file']" ), function( i, tag ) {
            $.each( $(tag)[0].files, function( i, file ) {
                dataForm.append( tag.name, file );
            });
        });


        var has_add_to_cart = false;
        data = form.serializeArray();

        $.each( data, function( i, val ) {
            if( val.name == 'add-to-cart' ) {
                has_add_to_cart = true;
            }
            dataForm.append( val.name, val.value );
        });
        dataForm.append( 'context', 'frontend' );
        dataForm.append( 'action', yith_wacp.actionadd );
        if( ! has_add_to_cart ) {
            dataForm.append('add-to-cart', form.find( 'button[name="add-to-cart"]').val());
        }

        $.ajax({
            url: yith_wacp.ajaxurl.toString().replace( '%%endpoint%%', yith_wacp.actionadd ),
            data: dataForm,
            contentType: false,
            processData: false,
            dataType: 'json',
            type: 'POST',
            success: function( res ) {

                // add error notice
                if( res.msg ) {

                    // add mess and scroll to Top
                    form.parents( 'div.product' ).before( res.msg );
                    $('body, html').animate({
                        scrollTop: 0
                    }, 500);

                    // reset button
                    button.removeAttr( 'disabled')
                        .removeClass( 'loading');

                    return false;
                }

                // refresh fragments
                var $ajax_url,
                    $data_ajax = {
                        product_id: res.prod_id,
                        variation_id: res.variation_id,
                        quantity: res.quantity,
                        ywacp_is_single: 'yes',
                        context: 'frontend'
                    };

                /** Support wc 2.3.x **/
                if( typeof wc_cart_fragments_params.wc_ajax_url !== 'undefined' ) {
                    $ajax_url = wc_cart_fragments_params.wc_ajax_url.toString().replace('%%endpoint%%', 'get_refreshed_fragments');
                }
                else {
                    $ajax_url = wc_cart_fragments_params.ajax_url;
                    $data_ajax.action = 'woocommerce_get_refreshed_fragments';
                }

                $.ajax({
                    url: $ajax_url,
                    type: 'POST',
                    data: $data_ajax,
                    success: function( data ) {
                        if ( data && data.fragments ) {

                            $.each( data.fragments, function( key, value ) {
                                $( key ).replaceWith( value );
                            });

                            if ( $supports_html5_storage ) {
                                sessionStorage.setItem( wc_cart_fragments_params.fragment_name, JSON.stringify( data.fragments ) );
                                sessionStorage.setItem( 'wc_cart_hash', data.cart_hash );
                            }

                            $( 'body' ).trigger( 'wc_fragments_refreshed' )
                                .trigger( 'wacp_single_added_to_cart' );

                            // remove disabled from submit button
                            button.removeAttr( 'disabled')
                                .removeClass( 'loading')
                                .addClass('added');
                        }
                    }
                });
            }
        });
    });

    $(document).on( 'yith_wacp_popup_after_opening yith_wacp_popup_changed', function() {
        if( typeof $.yith_wccl != 'undefined' && typeof $.fn.wc_variation_form != 'undefined' ) {
            // not initialized
            $(document).find( '.variations_form:not(.initialized)' ).each( function() {
                $(this).wc_variation_form();
            });
            $.yith_wccl();
        }

        // compatibility with lazyload
        if( typeof thb_lazyload != 'undefined' ) {
            thb_lazyload.update();
        }

    });
});