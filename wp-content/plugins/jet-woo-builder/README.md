# ChangeLog

## [1.0.2](https://github.com/ZemezLab/jet-woo-builder/archive/1.0.2.zip)
* Added: Ability to create templates based on pre-designed layouts

## [1.1.0](https://github.com/ZemezLab/jet-woo-builder/archive/1.1.0.zip)
* Added: Products Grid Widget
* Added: Products List Widget
* Added: Categories Grid Widget
* Added: Taxonomy Tiles