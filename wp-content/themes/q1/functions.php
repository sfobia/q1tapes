<?php
/**
 * Q1 Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Q1
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ACTIVE_NET_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'active-net-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ACTIVE_NET_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

function woocommerce_button_proceed_to_checkout() {
  $checkout_url = WC()->cart->get_checkout_url(); ?>

  <a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="checkout-button button alt wc-forward">
    <?php esc_html_e( 'Checkout', 'woocommerce' ); ?>
  </a>

  <?php
}


function an_title_order_received( $title, $id ) {
	if ( is_order_received_page() && get_the_ID() === $id ) {
		$title = "3. Thank you";
	}
	return $title;
}
add_filter( 'the_title', 'an_title_order_received', 10, 2 );


/* Mostra from: per il prezzo dei prodotti variabili */

function wc_wc20_variation_price_format( $price, $product ) {
  // Main prices
  $prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
  $price = $prices[0] !== $prices[1] ? sprintf( __( 'from %1$s', 'show-only-lowest-prices-in-woocommerce-variable-products' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
  // Sale price
  $prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
  sort( $prices );
  $saleprice = $prices[0] !== $prices[1] ? sprintf( __( 'from %1$s', 'show-only-lowest-prices-in-woocommerce-variable-products' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );
  if ( $price !== $saleprice ) {
      $price = '<del>' . $saleprice . '</del> <ins>' . $price . '</ins>';
  }
  return $price;
}
add_filter( 'woocommerce_variable_sale_price_html', 'wc_wc20_variation_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'wc_wc20_variation_price_format', 10, 2 );


/* Display download View for Current Post ID */
function sdcc_event_files() {
  global $post;
  $event_files_shortcode = "[wpv-view name=\"download\" ids=".$post->ID."]";
  return do_shortcode($event_files_shortcode);
}
add_shortcode( 'downloads', 'sdcc_event_files' );

// Ship to a different address closed by default
 
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );

/* Store locator */

add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}

add_filter ( 'woocommerce_account_menu_items', 'misha_remove_my_account_links' );
function misha_remove_my_account_links( $menu_links ){
 
	//unset( $menu_links['edit-address'] ); // Addresses 
	unset( $menu_links['dashboard'] ); // Dashboard	
	return $menu_links;
 
}

add_filter( 'woocommerce_continue_shopping_redirect', 'wc_custom_redirect_continue_shopping' );

function wc_custom_redirect_continue_shopping() {
  
  return "https://www.q1tapes.com/uk/painting/painting-renovation-range/";
}

/* Plugin Name: First name plus last name as default display name. */
add_action( 'user_register', 'wpse_67444_first_last_display_name' );

function wpse_67444_first_last_display_name( $user_id )
{
    $data = get_userdata( $user_id );
    // check if these data are available in your real code!
    wp_update_user( 
        array (
            'ID' => $user_id, 
            'display_name' => "$data->first_name $data->last_name"
        ) 
    );
}