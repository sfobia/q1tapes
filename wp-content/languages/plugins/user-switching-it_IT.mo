��          �      �       H     I     _  4   w     �  
   �     �     �     �          (  :   7     r  ;   �  %  �  0   �  ,   !  4   N     �     �     �     �     �     �               $  ;   @     
                            	                                Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://johnblackbourn.com/ https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2018-03-25 16:16:23+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: it
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Non è stato possibile uscire dal cambio utente. Non è stato possibile scambiare gli utenti. Consente il cambio istantaneo di utente in WordPress John Blackbourn Disconnettiti temporaneamente Torna a %1$s (%2$s) Cambia&nbsp;Utente Tornato a %1$s (%2$s). Cambiato a %1$s (%2$s). User Switching Cambio utente https://johnblackbourn.com/ https://johnblackbourn.com/wordpress-plugin-user-switching/ 